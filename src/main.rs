use crossterm::{cursor, terminal, ExecutableCommand};
use crossterm::event::{poll, read, Event, KeyCode};
use std::io::{stdout, Stdout, Write};
use std::time::Duration;
use std::collections::VecDeque;

#[derive(Debug)]
#[derive(Clone)]
enum Direction {
    Up,
    Right,
    Down,
    Left
}
#[derive(Debug)]
#[derive(Clone)]
struct Position {
    x :u16,
    y: u16
}

impl Position {
    fn new(x: u16, y :u16) -> Position {
        Position {x: x, y: y}
    }

}

#[derive(Debug)]
struct Snake {
    position :Position,
    direction: Direction,
    length: u8,
    body: VecDeque<Position>
}

impl Snake {
    fn new(at: Position) -> Snake {

        let mut snake = Snake {
            position: Position::new(at.x, at.y),
            direction: Direction::Right,
            length: 6,
            body: VecDeque::new()
        };

        snake.body.push_back(snake.position.clone());

        for n in 1..snake.length {

            let mut position_aux = Position::new(snake.position.x, snake.position.y);
            match snake.direction {
                Direction::Up => position_aux.y += n as u16,
                Direction::Right => position_aux.x -= n as u16,
                Direction::Down => position_aux.y -= n as u16,
                Direction::Left => position_aux.x += n as u16
            }
            snake.body.push_back(position_aux);
        }

        snake
    }

    fn draw(&self, stdout :&mut Stdout) {
        for (index, segment) in self.body.iter().enumerate() {
            stdout.execute(cursor::MoveTo(segment.x, segment.y)).unwrap();
            if index == 0 {
                write!(stdout, "O").unwrap();
            } else  if index == self.body.len() - 1 {
                write!(stdout, " ").unwrap();
            } else {
                write!(stdout, "o").unwrap();
            }
        }
    }
}

struct Game {
    snake: Snake,
    running: bool
}

impl Game {
    fn new(starting_point :Position) -> Game {
        Game {
            snake: Snake::new(starting_point),
            running: false
        }
    }

    fn start(&mut self, stdout :&mut Stdout) {
        self.running = true;

        loop {
            // `poll()` espera que se produzca un evento por un tiempo
            if poll(Duration::from_millis(50)).unwrap() {
                let event = read().unwrap();
                if event == Event::Key(KeyCode::Esc.into()) {
                    break;
                }

                if event == Event::Key(KeyCode::Up.into()) {
                    self.snake.direction = Direction::Up;
                }

                if event == Event::Key(KeyCode::Right.into()) {
                    self.snake.direction = Direction::Right;
                }
                if event == Event::Key(KeyCode::Down.into()) {
                    self.snake.direction = Direction::Down;
                }

                if event == Event::Key(KeyCode::Left.into()) {
                    self.snake.direction = Direction::Left;
                }

            }

            self.update(stdout);
            stdout.flush().unwrap();
        }   
    }

    fn update(&mut self, stdout :&mut Stdout) {
        match self.snake.direction {
            Direction::Up => self.snake.position.y -= 1,
            Direction::Right => self.snake.position.x += 1,
            Direction::Down => self.snake.position.y += 1,
            Direction::Left => self.snake.position.x -= 1
        }
        self.snake.body.pop_back().unwrap();
        self.snake.body.push_front(self.snake.position.clone());
        self.snake.draw(stdout);
        stdout.flush().unwrap();
    }

}

fn main() {

    // Habilitamos el modo raw del terminal, necesario más adelante para capturar el teclado    
    crossterm::terminal::enable_raw_mode().unwrap();
    
    // Borramos la pantalla
    let mut stdout = stdout();
    
    stdout.execute(terminal::Clear(terminal::ClearType::All)).unwrap();
    stdout.execute(cursor::Hide).unwrap();

    // Calculamos el puncto centro de la pantalla
    let size = terminal::size().unwrap();
    let center = Position::new(size.0 / 2, size.1 / 2);

    let mut game = Game::new(center);
    
    game.start(&mut stdout);
    

    // Llevamos el cursor a la última linea

    stdout.flush().unwrap();
    stdout.execute(cursor::MoveTo(0, size.1 - 1)).unwrap();

    // Devolvemos el terminal a su estado anterior
    crossterm::terminal::disable_raw_mode().unwrap();


}
